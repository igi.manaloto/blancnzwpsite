/* eslint-disable */

const timeStamp = '20180723_012037';

importScripts(
  'https://storage.googleapis.com/workbox-cdn/releases/3.4.1/workbox-sw.js'
);

// Prepare the offline page
workbox.precaching.precache([
  {
    url: '/offline',
    revision: '31072018',
  }
]);

const cacheKeys = {
  scripts: `blanc_scripts_cache_${timeStamp}`,
  images: `blanc_images_cache_${timeStamp}`,
  pages: `blanc_pages_${timeStamp}`,
};

const validCaches = Object.assign(
  {},
  workbox.core.cacheNames,
  cacheKeys
);

const threeDays = 3 * 24 * 60 * 60;

const imagesExpiry = new workbox.expiration.Plugin({
  maxAgeSeconds: threeDays,
  maxEntries: 60
});

const scriptsExpiry = new workbox.expiration.Plugin({
  maxAgeSeconds: threeDays,
  maxEntries: 30
});

const imagesStrategy = workbox.strategies.cacheFirst({
  cacheName: cacheKeys.images,
  plugins: [imagesExpiry]
});

// Use cache but update in the background ASAP
const scriptsStrategy = workbox.strategies.cacheFirst({
  cacheName: cacheKeys.scripts,
  plugins: [scriptsExpiry]
});

// Show cached first, and update it on the background
const apiStrategy = workbox.strategies.staleWhileRevalidate({
  cacheName: cacheKeys.scripts
});

workbox.routing.registerRoute(/.*\.(?:js|css)/, scriptsStrategy);
workbox.routing.registerRoute(/.*\/api\/.*/, apiStrategy);
workbox.routing.registerRoute(
  /.*\.(?:png|jpg|jpeg|svg|gif|woff2)/,
  imagesStrategy
);

const networkFirstHandler = workbox.strategies.networkFirst({
  cacheName: cacheKeys.pages,
  plugins: [
    new workbox.expiration.Plugin({
      maxEntries: 20
    }),
    new workbox.cacheableResponse.Plugin({
      statuses: [200]
    })
  ]
});

const matcher = ({event}) => event.request.mode === 'navigate';
const handler = (args) => networkFirstHandler.handle(args).then((response) => (!response) ? caches.match('/offline') : response);

workbox.routing.registerRoute(matcher, handler);

// clean up old SW caches
self.addEventListener("activate", function(event) {
  event.waitUntil(
    caches.keys().then(function(cacheNames) {
      let validCacheSet = new Set(Object.values(validCaches));
      return Promise.all(
        cacheNames
          .filter(function(cacheName) {
            return !validCacheSet.has(cacheName);
          })
          .map(function(cacheName) {
            console.info("deleting cache", cacheName);
            return caches.delete(cacheName);
          })
      );
    })
  );
});
